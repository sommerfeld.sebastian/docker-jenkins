#!/usr/bin/env groovy

final String KEEP_BUILDS="8"

final String branchName="feat/initialize-repository"
final String commitMessage="feat: initialized filesystem structure for this repository"
final String templateRepoGroup="sommerfeld.sebastian"
final String templateRepoName="docker-jenkins"

/**
 * Check if pipeline parameter is empty and if so, fail pipeline because parameter is always mandatory.
 * Determined via trim (Groovy truth -> if empty then trim returns false).
 *
 * @param param
 * @return
 */
def verifyPipelineParam(final GString param) {
    if(!param?.trim()) {
        currentBuild.result = "FAILURE"
        echo "FAIL PIPELINE"
        echo "Note that the very first run of the pipeline always fails because 'Build with parameters' is not available yet"
        error("At least onw parameter is empty ... All pipeline parameters are mandatory!")
    }
}

/**
 * Replace string in given file.
 *
 * @param filename
 * @param placeholder
 * @param newValue
 * @return
 */
def replaceInFile(final String filename, final String placeholder, final GString newValue) {
    def text = readFile file: filename
    text = text.replaceAll(placeholder, newValue)
    writeFile file: filename, text: text
}

/**
 * Perform cleanup tasks for this pipeline.
 *
 * @param reposDir
 * @param templateRepo
 * @return
 */
def cleanup(final String reposDir, final String templateRepo) {
    dir(reposDir) {
        sh "rm -rf ${PROJECT_NAME}"
        sh "rm -rf $templateRepo"
    }
}

/**
 * The pipeline itself
 */
pipeline {
    agent any

    options {
        buildDiscarder(logRotator(numToKeepStr: "$KEEP_BUILDS", artifactNumToKeepStr: "$KEEP_BUILDS"))
    }

    environment {
        REPOS_DIR = "${env.WORKSPACE}/tmp/repos"
    }

    parameters {
        string(name: "PROJECT_GROUP", defaultValue: "sommerfeld.sebastian", description: "Group containing the repository from gitlab.com")
        string(name: "PROJECT_NAME", description: "Repository name from gitlab.com")
        string(name: "PROJECT_TITLE", description: "Readable title for Antora docs")
        string(name: "PROJECT_EMAIL", description: "Incident email for this repository from gitlab.com (see Incidents > Service Desk)")
    }

    stages {
        stage("verify") {
            // Adjust IntelliJ validations
            // noinspection GroovyAssignabilityCheck
            parallel {
                stage("info") {
                    steps {
                        sh "printenv"
                        echo "pipeline param: PROJECT_GROUP = ${PROJECT_GROUP}" // __PROJECT_GROUP__
                        echo "pipeline param: PROJECT_NAME  = ${PROJECT_NAME}"  // __PROJECT_NAME__
                        echo "pipeline param: PROJECT_TITLE = ${PROJECT_TITLE}" // __PROJECT_TITLE__
                        echo "pipeline param: PROJECT_EMAIL = ${PROJECT_EMAIL}" // __PROJECT_EMAIL__
                        echo "REPOSITORY = https://gitlab.com/${PROJECT_GROUP}/${PROJECT_NAME}..git"
                        echo "REPOSITORY = git@gitlab.com:${PROJECT_GROUP}/${PROJECT_NAME}..git"
                    }
                }
                stage("check parameters") {
                    steps {
                        echo "Verify if params are empty and if so -> fail pipeline"
                        verifyPipelineParam("${PROJECT_GROUP}")
                        verifyPipelineParam("${PROJECT_NAME}")
                        verifyPipelineParam("${PROJECT_TITLE}")
                        verifyPipelineParam("${PROJECT_EMAIL}")
                    }
                }
            }
        }

        stage("prepare::filesystem") {
            steps {
                script {
                    cleanup(REPOS_DIR.toString(), templateRepoName)
                }
                sh "mkdir -p ${REPOS_DIR}"
            }
        }

        stage("prepare::git") {
            // Adjust IntelliJ validations
            // noinspection GroovyAssignabilityCheck
            parallel {
                stage("clone target repo") {
                    steps {
                        dir("${REPOS_DIR}") {
                            sh "git clone git@gitlab.com:${PROJECT_GROUP}/${PROJECT_NAME}.git"
                        }

                        dir("${REPOS_DIR}/${PROJECT_NAME}") {
                            sh "git checkout -b $branchName"
                        }
                    }
                }
                stage("clone template repo") {
                    steps {
                        dir("${REPOS_DIR}") {
                            sh "git clone git@gitlab.com:${templateRepoGroup}/${templateRepoName}.git"
                        }
                    }
                }
            }
        }

        stage("build") {
            // Adjust IntelliJ validations
            // noinspection GroovyAssignabilityCheck
            parallel {
                stage("gitlab.com") {
                    stages {
                        stage('setup labels') {
                            steps {
                                echo "TODO" // todo ...
                            }
                        }
                        stage('setup board') {
                            steps {
                                echo "TODO" // todo ...
                            }
                        }
                    }
                }
                stage("project") {
                    stages {
                        stage('filesystem structure') {
                            steps {
                                dir("${REPOS_DIR}/${PROJECT_NAME}") {
                                    sh """
                                        rm -rf *
                                        cp -a ${REPOS_DIR}/${templateRepoName}/resources/project-layout-default .
                                        cp ${REPOS_DIR}/${templateRepoName}/resources/project-layout-default/.folderslintrc .
                                        cp ${REPOS_DIR}/${templateRepoName}/resources/project-layout-default/.gitignore .
                                        cp ${REPOS_DIR}/${templateRepoName}/resources/project-layout-default/.gitlab-ci.yml .
                                        cp ${REPOS_DIR}/${templateRepoName}/resources/project-layout-default/.gitpod.yml .
                                        cp ${REPOS_DIR}/${templateRepoName}/resources/project-layout-default/.yamllint .
                
                                        (
                                            cd project-layout-default || exit
                                            mv * ../
                                            cd .. || exit
                                            rm -rf project-layout-default
                                        )
                                        
                                        echo "-----  ${REPOS_DIR}/${PROJECT_NAME}  -----------------------------------------------"
                                        ls -alF
                                    """
                                }
                            }
                        }
                        stage('replace placeholders') {
                            steps {
                                dir("${REPOS_DIR}/${PROJECT_NAME}") {
                                    script {
                                        def files = [
                                                "docs/antora.yml",
                                                'docs/modules/ROOT/pages/index.adoc'
                                        ]
                                        files.each {
                                            String filename = it
                                            replaceInFile(filename, "__PROJECT_GROUP__", "${PROJECT_GROUP}")
                                            replaceInFile(filename, "__PROJECT_NAME__", "${PROJECT_NAME}")
                                            replaceInFile(filename, "__PROJECT_TITLE__", "${PROJECT_TITLE}")
                                            replaceInFile(filename, "__PROJECT_EMAIL__", "${PROJECT_EMAIL}")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        stage("release") {
            steps {
                dir("${REPOS_DIR}/${PROJECT_NAME}") {
                    sh """
                        pwd
                        ls -alF

                        git add ./*
                        git add .
                        git commit -m '$commitMessage'
                        git push origin $branchName
                    """
                }
            }
        }

        stage("clean") {
            steps {
                script {
                    cleanup(REPOS_DIR.toString(), templateRepoName)
                }
            }
        }
    }
}
