#!/bin/bash
# @file build.sh
# @brief Build docker image from project docker-jenkins.
#
# @description The script builds a docker image to use as jenkins slave agent. This image adds Antora to the
# ``asciidoctor/docker-asciidoctor`` base image.
#
# The ``asciidoctor/docker-asciidoctor`` image uses Alpine Linux 3.13.7 as base image and ships with ...
#
# * Asciidoctor
# * Asciidoctor Diagram with ERD and Graphviz integration (supports plantuml and graphiz diagrams)
# * Asciidoctor PDF
# * Asciidoctor EPUB3
# * Asciidoctor FB2
# * Asciidoctor Mathematical
# * Asciidoctor reveal.js
# * AsciiMath
# * Source highlighting using Rouge, CodeRay or Pygments
# * Asciidoctor Confluence
# * Asciidoctor Bibtex
# * Asciidoctor Kroki
#
# IMPORTANT: TODO: Install Antora !!!
#
# ==== Arguments
#
# The script does not accept any parameters.
#
# ==== See also
#
# * link:https://github.com/asciidoctor/docker-asciidoctor[asciidoctor/docker-asciidoctor on Github]
# * link:https://hub.docker.com/r/asciidoctor/docker-asciidoctor[asciidoctor/docker-asciidoctor on DockerHub]

echo -e "$LOG_INFO Lint Dockerfile"
docker run --rm -i hadolint/hadolint < Dockerfile

echo -e "$LOG_INFO Build image"
docker build -t kobol/jenkins-agent-asciidoc:latest .

echo -e "$LOG_DONE Finished docker build"
