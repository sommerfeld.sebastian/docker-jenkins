#!/usr/bin/env groovy
import jenkins.model.Jenkins

def systemMessage = "For further information see Docker Jenkins Docs at https://www.sommerfeld.io/docs/docker-jenkins-docs/main"

Jenkins jenkins = Jenkins.getInstance()
jenkins.setSystemMessage(systemMessage)
jenkins.save()
