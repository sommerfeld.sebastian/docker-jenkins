#!/usr/bin/env groovy

/**
 * Setup jobs from filesystem
 * @return
 */
def createJobsFromFilesystem() {
    String rootFolder = "/var/jenkins_home/jobs/SEED/workspace/pipelines"
    dh = new File(rootFolder)
    dh.eachFile {

        String folderName = it.getName()

        pipelineJob(folderName) {
            definition {
                cps {
                    script(readFileFromWorkspace(rootFolder + "/" + folderName + "/Jenkinsfile"))
                    sandbox()
                }
            }
        }
    }
}

createJobsFromFilesystem()
