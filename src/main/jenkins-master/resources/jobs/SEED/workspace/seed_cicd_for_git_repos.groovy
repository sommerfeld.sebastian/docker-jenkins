#!/usr/bin/env groovy

/**
 * Setup default cicd job from src/cicd/Jenkinsfile folder of gitlab project.
 * Add new repositories to list at the end of the file.
 * @param repo The repository for which jobs are created
 * @param path The path to the Jenkinsfile (defaults to cicd)
 * @return
 */
def createPipelineJob(final String repo, final String path = 'cicd') {

    final String groupName = repo.substring(0, repo.lastIndexOf("/"))
    final String repoName = repo.substring(repo.lastIndexOf("/") + 1, repo.length())
    final String jobName = groupName + "___" + repoName + "___" + path.replace("/", "___")

    pipelineJob(jobName) {
        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url("git@gitlab.com:" + repo +".git")
                        }

                        branches("*/main")
                        branches("*/master")
                    }
                }

                scriptPath("src/${path}/Jenkinsfile")
            }
        }
    }
}

createPipelineJob("sommerfeld.sebastian/docker-jenkins", "cicd/jenkins-master/BUILD_snapshot_nightly")
createPipelineJob("sommerfeld.sebastian/docker-jenkins", "cicd/jenkins-agent-asciidoc/BUILD_snapshot_nightly")
createPipelineJob("sommerfeld.sebastian/docker-jenkins", "cicd/utils/ADMIN_initialize_repository")
createPipelineJob("sommerfeld.sebastian/docker-vagrant", "cicd/BUILD_snapshot_nightly")
createPipelineJob("sommerfeld.sebastian/kobol", "cicd")
createPipelineJob("sommerfeld.sebastian/homelab-config", "cicd")
createPipelineJob("sommerfeld.sebastian/spring-rest-docs-example", "cicd/BUILD_AND_DEPLOY_snapshot_nightly")
createPipelineJob("sommerfeld.sebastian/website-masterblender-de", "cicd/DEPLOY")
createPipelineJob("sommerfeld.sebastian/website-numero-uno-de", "cicd/DEPLOY")
createPipelineJob("sommerfeld-io/ui-bundle-blog", "cicd/BUILD_snapshot_nightly")
createPipelineJob("sommerfeld-io/ui-bundle-blog", "cicd/RELEASE")
createPipelineJob("sommerfeld-io/ui-bundle-docs", "cicd/BUILD_snapshot_nightly")
createPipelineJob("sommerfeld-io/ui-bundle-docs", "cicd/RELEASE")
createPipelineJob("sommerfeld-io/website", "cicd/DEPLOY_prod")
createPipelineJob("sommerfeld-io/website", "cicd/GENERATE_bash_api_docs")
createPipelineJob("sommerfeld-io/website", "cicd/GENERATE_website_content")
