#!/bin/bash
# @file stop.sh
# @brief Stop all containers of the Jenkins environment.
#
# @description The script stops all containers of the Jenkins environment. All containers, networks and volumes are removed after shutdown.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Shutting down"
docker-compose down -v

echo -e "$LOG_DONE Stopped and removed containers, networks and volumes"
