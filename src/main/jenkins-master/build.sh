#!/bin/bash
# @file build.sh
# @brief Build docker image from project docker-jenkins.
#
# @description The script lints the Dockerfile and builds the image ``jenkins-master`` from this project to use as a
# Jenkins master instance. Configuration, plugins and SEED jobs are built into this image so it is ready to use.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Lint Dockerfile"
docker run --rm -i hadolint/hadolint < Dockerfile

echo -e "$LOG_INFO Build image"
docker build -t kobol/jenkins-master:latest .

echo -e "$LOG_DONE Finished docker build"
