#!/bin/bash
# @file start.sh
# @brief Run Jenkins environment in containers using the image built by ``build.sh`` script.
#
# @description The script starts a Jenkins master instance in a container using docker-compose and the previously
# built image ``jenkins-master`` from this repo. The Containers are started in the background. After stopping Jenkins,
# the container is removed as well.
#
# | What                               | Port | Protocol |
# | ---------------------------------- | ---- | -------- |
# | Jenkins Web UI                     | 9080 | http     |
# | Jenkins Slave Agent                | 9081 | http     |
#
# ===== Use SSH Keys from host
#
# By sharing the unix socket ``ssh auth sock`` (mounted as volume) and setting the ``SSH_AUTH_SOCK`` environment
# variable, the Jenkins container is able to use ssh keys from the host machine and hence is able to Jenkins to clone
# from and push to gitlab.com and github.com. To ensure the gitlab.com and github.com hosts are known, the
# ``$HOME/.ssh/known_hosts`` is mountet as well (it is assumed, that github.com and gitlab.com are known to the host).
# Since the volumes are mounted at runtime, there is no need for ssh keys inside the container.
#
# ===== Docker
#
# The docker-compose.yml mounts Docker into the container. This allows the ``jenkins-master`` container to use the
# Docker Engine from the host machine. To use the docker cli, a cli installation is present inside the image.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO ========== Variables ===================================================="
echo -e "$LOG_INFO HOME .................. $HOME"
echo -e "$LOG_INFO SSH_AUTH_SOCK  ........ $SSH_AUTH_SOCK"
echo -e "$LOG_INFO ========================================================================="

echo -e "$LOG_INFO Export vars for docker-compose"
export PARAM_HOME="$HOME"
export PARAM_SSH_AUTH_SOCK="$SSH_AUTH_SOCK"

echo -e "$LOG_INFO Starting containers"
docker-compose up -d

echo -e "$LOG_INFO Startup in progress (detached)"
