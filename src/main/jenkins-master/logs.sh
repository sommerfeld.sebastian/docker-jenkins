#!/bin/bash
# @file logs.sh
# @brief Show logs for containers.
#
# @description Show logs for containers.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Showing logs"
docker-compose logs -f
