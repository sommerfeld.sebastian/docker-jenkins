FROM jenkins/jenkins:2.321
LABEL maintainer="sebastian@sommerfeld.io"

# Install docker client only
# Use docker from host machine: --volume /var/run/docker.sock:/var/run/docker.sock \
USER root
RUN apt-get update \
    && apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release \
    && curl -fsSL "https://download.docker.com/linux/debian/gpg" | gpg --dearmor -o "/usr/share/keyrings/docker-archive-keyring.gpg" \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt-get update \
    && apt-get install -y docker-ce-cli

USER "${uid}:${gid}"

# git installation is shipped with the base image
RUN git config --global user.email 'sommerfeld.sebastian@gmail.com' \
    && git config --global user.name 'sebastian' \
    && git config --global credential.helper cache \
    && git config --global credential.helper 'cache --timeout=3600' \
    && git config --global pull.rebase false

COPY resources/plugins.list /usr/share/jenkins/ref/plugins.list
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.list

COPY resources/secrets /var/jenkins_home/secrets

COPY --chown=jenkins:jenkins resources/init.groovy.d /usr/share/jenkins/ref/init.groovy.d
COPY resources/jenkins-as-code-config.yml /var/jenkins_home/jenkins-as-code-config.yml
COPY resources/jobs/SEED /usr/share/jenkins/ref/jobs/SEED

ENV JENKINS_USER "admin"
ENV JENKINS_PASS "admin"

ENV JAVA_OPTS="-Xmx4096m"
ENV JENKINS_OPTS=" --handlerCountMax=300"

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV CASC_JENKINS_CONFIG /var/jenkins_home/jenkins-as-code-config.yml
